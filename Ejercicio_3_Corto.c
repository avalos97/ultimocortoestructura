#include<stdio.h>
#include<stdlib.h>

/**
 * Declaramos una estructura que define cada elemento
 * que componen nuestra pila
*/
struct nodo{
    int info;
    struct nodo *sig;
};


/**
 * declaramos un puntero que apunta al primer elemento
 * iniciandolo en nulo, cuando esta vacia la pila
*/

struct nodo *raiz=NULL;

void insertar(int x){//funcion para crear nodo en nuestra pila, y al mismo tiempo ingresar los datos, recibiendo de parametro un entero
    
    struct nodo *nuevo;
    nuevo = malloc(sizeof (struct nodo));//Hacemos uso de malloc por trabajar memoria dinamica
    nuevo->info = x;
    
    if (raiz == NULL) {//validamos que la raiz sea nula
        raiz=nuevo;
        nuevo->sig=NULL;
    }else
    {
        nuevo->sig = raiz;//decimos que ahora el dato "raiz" sera el siguiente que extraeremos despues del que acabamos de ingresar
        raiz=nuevo;//el dato ingresado pasa a ser raiz, y a estar en la cima de la pila
        
    }   
    
}
void imprimir(){//funcion para imprimir datos de la pila
    struct nodo *reco=raiz;
    while(reco!=NULL){//esto mientras la pila aun tenga datos que mostrar
        printf("%i ", reco->info);//imprimimos la pila
        reco=reco->sig;
        
    }
    printf("\n");
}


/* 
función Reemplazar que tiene como argumentos una pila con
tipo de elemento int y dos valores int: nuevo y viejo de forma que si el
primer valor aparece en algún lugar de la pila,sea reemplazado por el
segundo.
*/
void reemplazar(struct nodo *pila, int viejo, int nuevo ){
    
    if(pila != NULL){
        while(pila!=NULL){
        if(pila->info == viejo){
            pila->info = nuevo;
        }
        pila = pila->sig;
        }
    }else{
        printf("No se encontro coincidencia con la pila\n");
    }

}

int main(){
    int viejo,nuevo;

    //Ingresamos datos a la pila atravez de la funcion insertar
    insertar(10);
    insertar(40);
    insertar(3);
    insertar(7);
    insertar(3);

    printf("\nLista inicial:\n");
    //Imprimimos los datos de la pila atravez de la funcion imprimir  
    imprimir();
    printf("Ingrese el numero Viejo:\n");
    scanf("%d", &viejo);
    printf("Ingrese el numero Nuevo:\n");
    scanf("%d", &nuevo);
    reemplazar(raiz,viejo, nuevo);//pasamos los parametros ingresados a la funcion reemplazar
    printf("\nSe sustituyo el Viejo = %d por el Nuevo = %d",viejo,nuevo);
    printf("\nLa lista queda:\n");
    //Imprimimos los datos de la pila atravez de la funcion imprimir  
    imprimir();
    
  return 0;
}
