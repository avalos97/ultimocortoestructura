#include <stdlib.h>
#include <stdio.h>

/**
 * Declaramos una estructura que define cada elemento
 * que componen nuestra pila
*/
typedef struct nodo 
{
    int dato;
    struct nodo *siguiente;
} nodo;

/**
 * declaramos un puntero que apunta al primer elemento
 * iniciandolo en nulo, cuando esta vacia la pila
*/
nodo *primero = NULL;

int main(void){
    
/**
 * Para mostrar nuetros dígitios ingresados tal cual(En orden),
 * haremos uso de un array y para mostrar nuestros dígitos en forma invertida
 * haremos uso de "Pilas".
 * Se procedera a llenar al mismo tiempo el vector y la pila.
*/
int tamanio;
    printf("\nBuen día Esperanza.\n");//Antes que nada, el Saludo xD
    printf("\nCuantos Digitos ingresará?:\n");
    scanf("%d", &tamanio);
    int vector[tamanio];//declaramos el vector con su tamaño

    
    
    for(int i = 0; i <tamanio; i++){//bucle para proceder al llenado de la pila y vector
    nodo *nuevo = (nodo *)malloc(sizeof(nodo));//Hacemos uso de malloc por trabajar memoria dinamica
    printf("Ingrese el dígito #%d:\n", i+1);
    scanf("%d", &nuevo->dato);
    vector[i]=nuevo->dato;
    nuevo->siguiente = primero;//decimos que ahora el dato "primero" sera el siguiente que extraeremos despues del que acabamos de ingresar
    primero = nuevo;//el dato ingresado pasa a ser primero, y a estar en la cima de la pila
    
    }

    printf("\nLos dígitos en orden son:\n");
    
    for(int i = 0; i < tamanio; i++){//Procedemos a impreimir los digitos ordenadamente
        printf("%d\t", vector[i]);
    }   

    desplegarPila();//llamamos la funcion para mostrar la pila

}


void desplegarPila(){
    struct nodo *actual=primero;

        printf("\nLos dígitos invertidos atravez de la pila es:\n");
        while (actual != NULL)//esto mientras la pila aun tenga datos que mostrar
        {
            printf("%d\t", actual->dato);//imprimimos la pila, para invertir los dígitos
            actual = actual->siguiente;
            
        }
        printf("\n");
    

}

